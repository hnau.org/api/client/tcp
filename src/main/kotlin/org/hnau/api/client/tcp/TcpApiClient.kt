package org.hnau.api.client.tcp

import org.hnau.api.client.common.ApiCall
import org.hnau.api.client.common.ApiClientEngine
import org.hnau.base.data.bytes.provider.toBytesProvider
import org.hnau.base.data.bytes.receiver.toBytesReceiver
import org.hnau.base.data.bytes.receiver.write
import org.hnau.base.data.time.Time
import org.hnau.base.data.time.asSeconds
import java.net.Socket


fun TcpApiClient(
        host: String,
        port: Int,
        timeout: Time = 10.asSeconds
) = object : ApiClientEngine {

    override suspend fun <O> call(
            call: ApiCall<O>
    ): O {
        val socket = Socket(host, port)
        socket.soTimeout = timeout.milliseconds.toInt()
        val outputStream = socket.getOutputStream()
        call.writeRequest(outputStream.toBytesReceiver())
        outputStream.flush()
        val inputStream = socket.getInputStream()
        val response = call.readResponse(inputStream.toBytesProvider())
        inputStream.close()
        outputStream.close()
        socket.close()
        return response
    }

}