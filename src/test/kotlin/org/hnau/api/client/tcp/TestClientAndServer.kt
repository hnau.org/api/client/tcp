package org.hnau.api.client.tcp

import kotlinx.coroutines.runBlocking
import org.hnau.api.client.common.ApiCall
import org.hnau.api.server.tcp.TcpApiServer
import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.bytes.provider.read
import org.hnau.base.data.bytes.provider.readInt
import org.hnau.base.data.bytes.provider.readString
import org.hnau.base.data.bytes.receiver.BytesReceiver
import org.hnau.base.data.bytes.receiver.write
import org.hnau.base.data.bytes.receiver.writeInt
import org.hnau.base.data.bytes.receiver.writeString
import org.junit.Test
import org.assertj.core.api.Assertions.assertThat


class TestClientAndServer {

    private data class TestData(
            val a: Int,
            val b: String
    ) {

        companion object {

            val bytesAdapter = BytesAdapter(
                    read = {
                        TestData(
                                a = readInt(),
                                b = readString()
                        )
                    },
                    write = { data ->
                        writeInt(data.a)
                        writeString(data.b)
                    }
            )

        }

    }

    @Test
    fun test() {

        val port = 52362

        val closeServer = TcpApiServer(
                port = port,
                onThrowableWasThrownWhenHandlingClient = { throw it },
                handler = {
                    val data = read(TestData.bytesAdapter.read)
                    val result: BytesReceiver.() -> Unit = {
                        write(data, TestData.bytesAdapter.write)
                    }
                    result
                }
        )

        val client = TcpApiClient(
                host = "127.0.0.1",
                port = port
        )

        runBlocking {

            val initialData = TestData(
                    a = -345,
                    b = "QWERTY"
            )

            val response = client.call(
                    ApiCall<TestData>(
                            writeRequest = {
                                write(initialData, TestData.bytesAdapter.write)
                            },
                            readResponse = {
                                read(TestData.bytesAdapter.read)
                            }
                    )
            )

            assertThat(response).isEqualTo(initialData)

        }

        closeServer()

    }

}